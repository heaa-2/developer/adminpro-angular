import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './login/register.component';
// Pages
import {PagesModule} from './pages/pages.module';
// Routes
import { APP_ROUTES } from './app.route';
// Services
import {ServicesModule} from './services/services.module';
// Guards
import {GuardsModule} from './guards/guards.module';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {CommonModule} from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent
  ],
    imports: [
        CommonModule,
        FormsModule,
        BrowserModule,
        PagesModule,
        ServicesModule,
        GuardsModule,
        ReactiveFormsModule,
        HttpClientModule,
        APP_ROUTES,
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
