import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import Swal from 'sweetalert2';
import {UserService} from '../services';
import {User} from '../models/user.model';
import {Router} from '@angular/router';
declare function init_pluging();

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./login.component.css']
})
export class RegisterComponent implements OnInit {
  formRegister: FormGroup;
  constructor(
    private userService: UserService,
    private router: Router,
  ) { }

  isEquals( p1: string, p2: string ) {
    return (group: FormGroup) => {
      const field1 = group.controls[p1].value;
      const field2 = group.controls[p2].value;
      if (field1 === field2) {
        return null;
      }
      return { isEquals: true };

    };
  }

  ngOnInit(): void {
    init_pluging();
    this.formRegister = new FormGroup({
      name: new FormControl(null, Validators.required),
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, Validators.required),
      password2: new FormControl(null, Validators.required),
      condition: new FormControl(false),
    }, {
      validators: this.isEquals('password', 'password2')
    });
    /*
    this.formRegister.setValue({
      name: 'test',
      email: 'email@email.com',
      password: '12345',
      password2: '1234',
      condition: true,
    });
     */
  }

  ngRegisterUser()  {
    if (!this.formRegister.valid) {
      Swal.fire({
        title: 'Error!',
        text: 'por favor ingrese los datos solicitados',
        icon: 'error',
        confirmButtonText: 'Cool'
      });
      return false;
    }
    const user = new User( this.name.value, this.email.value, this.password.value);
    this.userService.create(user)
      .subscribe(res => this.router.navigate(['/login']), err => console.log(err));
  }
  get name()        { return this.formRegister.get('name'); }
  get email()       { return this.formRegister.get('email'); }
  get password()    { return this.formRegister.get('password'); }
  get password2()   { return this.formRegister.get('password2'); }
  get condition()   { return this.formRegister.get('condition'); }
}
