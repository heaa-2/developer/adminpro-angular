import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import Swal from 'sweetalert2';
import {User} from '../models/user.model';
import {AuthService} from '../services';

declare function init_pluging();

declare const gapi: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  remember = false;
  email: string;
  auth2: any;

  constructor(
    private route: Router,
    private authService: AuthService
  ) {
  }

  ngOnInit(): void {
    init_pluging();
    this.googleInit();

    this.email = localStorage.getItem('email') || '';
    this.remember = (this.email.length > 0);
  }

  login(form: NgForm) {

    if (!form.valid) {
      Swal.fire({
        title: 'Error!',
        text: 'por favor ingrese los datos solicitados',
        icon: 'error',
        confirmButtonText: 'Cool'
      });
      return false;
    }

    const user = new User(null, form.value.email, form.value.password);
    this.authService.auth(user, form.value.remember).subscribe(res => this.route.navigate(['dashboard']), err => {
      Swal.fire({
        title: 'Error!',
        text: 'Usuario no encontrado',
        icon: 'error',
        confirmButtonText: 'Cool'
      });
    });


  }


  googleInit() {

    gapi.load('auth2', () => {

      this.auth2 = gapi.auth2.init({
        client_id: '884863477420-sng908gn1k1fm9aua035bi97kasr7qnb.apps.googleusercontent.com',
        cookiepolicy: 'single_host_origin',
        scope: 'profile email'
      });

      this.attachSignin(document.getElementById('btnGoogle'));

    });

  }

  attachSignin(element) {
    this.auth2.attachClickHandler(element, {}, (googleUser) => {
      // let profile = googleUser.getBasicProfile();
      const token = googleUser.getAuthResponse().id_token;
      this.authService.authGoogle(token).subscribe(() => window.location.href = '#/dashboard');
    });

  }
}
