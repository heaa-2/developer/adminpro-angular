import { Component, OnInit, OnDestroy } from '@angular/core';
import {Observable, Subscriber, Subscription} from 'rxjs';
import {map, retry, filter} from 'rxjs/operators';

@Component({
  selector: 'app-rxjs',
  templateUrl: './rxjs.component.html',
  styles: []
})
export class RxjsComponent implements OnInit, OnDestroy {

  sub: Subscription;
  constructor() {

    this.sub = this.returnObserver().subscribe(
      num => console.log('sub', num),
      error => console.log('Error', error),
      () => console.log('Termino!!!')
    );
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
  returnObserver(): Observable<any> {
    return new Observable( (observer: Subscriber<any>) => {
      let contador = 0;
      const clock = setInterval(() => {
        contador++;
        const retorno = {
          contador
        };
        observer.next(retorno);
        /*
        if (contador === 3) {
          clearInterval(clock);
          observer.complete();
        }

        if (contador === 2 ) {
          clearInterval(clock);
          observer.error('Ayuda!!!');
        }
        */
      }, 1000);
    }).pipe(
      map(resp => resp.contador),
      filter((value, key) => {
        if ( (value % 2)  === 1) {
          return true;
        }
        return false;
      })
    );

  }

}
