import {RouterModule, Routes} from '@angular/router';
import {PagesComponent} from './pages.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {ProgressComponent} from './progress/progress.component';
import {Grafica1Component} from './grafica1/grafica1.component';
import {AccountSettingsComponent} from './account-settings/account-settings.component';
import {PromisesComponent} from './promises/promises.component';
import {ProfileComponent} from './profile/profile.component';
import {RxjsComponent} from './rxjs/rxjs.component';
import {AuthGuard} from '../guards';


const pagesRoutes: Routes = [

  {
    path: '',
    component: PagesComponent,
    canActivate : [AuthGuard],
    children: [
      {path: 'dashboard', component: DashboardComponent, data: { title: 'Dashboard' }},
      {path: 'progress', component: ProgressComponent, data: { title: 'Progress' }},
      {path: 'grafica1', component: Grafica1Component, data: { title: 'Graficos' }},
      {path: 'account-settings', component: AccountSettingsComponent, data: { title: 'Ajustes de Cuenta' }},
      {path: 'promesas', component: PromisesComponent, data: { title: 'Promesas' }},
      {path: 'profile', component: ProfileComponent, data: { title: 'Perfil De Usuario' }},
      {path: 'rxjs', component: RxjsComponent, data: { title: 'RxJs' }},
      {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
    ]
  }
];
export const PAGES_ROUTES = RouterModule.forChild(pagesRoutes);
