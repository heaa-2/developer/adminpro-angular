import {Component, OnInit} from '@angular/core';
import {User} from '../../models/user.model';
import {UserService} from '../../services';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styles: []
})
export class ProfileComponent implements OnInit {
  user: User;
  uploadFile: File;
  imagenTemp: string;

  constructor(
    public userService: UserService
  ) {
  }

  ngOnInit(): void {
    this.user = this.userService.user;
  }

  submit(user: User) {
    this.user.name = user.name;
    if (!this.user.google) {
      this.user.email = user.email;
    }
    new Promise((resolve, reject) => {
      this.userService.update(this.user).subscribe(res => resolve(res), err => reject(err));
    }).then(res => {
      Swal.fire({
        title: 'Actualizado!',
        text: 'Usuario Actualizado',
        icon: 'success',
        confirmButtonText: 'Ok'
      });
    }).catch(err => {
      Swal.fire({
        title: 'Actualizado!',
        text: 'Usuario No se pudo Actualizar',
        icon: 'error',
        confirmButtonText: 'Ok'
      });
    });

  }

  selectImg(file: File) {
    if (!file) {
      this.uploadFile = null;
      return;
    }

    if (file.type.indexOf('image') < 0) {
      Swal.fire({
        title: 'Error!',
        text: 'Solo se permiten imagenes',
        icon: 'error',
        confirmButtonText: 'Ok'
      });
      this.uploadFile = null;
      return;
    }
    this.uploadFile = file;
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => this.imagenTemp = reader.result as string;
  }

  async uploadImg() {
    const res: any =  await this.userService.updateAvatar(this.uploadFile, this.user._id);
    console.log(res);
  }
}
