import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-progress',
  templateUrl: './progress.component.html',
  styles: []
})
export class ProgressComponent implements OnInit {
  percentageOne: number;
  percentageTwo: number;
  min: number;
  max: number;

  constructor() {
    this.percentageOne = 50;
    this.percentageTwo = 20;
    this.min = 0;
    this.max = 100;
  }

  ngOnInit(): void {
  }

}
