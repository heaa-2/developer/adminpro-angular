import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-promises',
  templateUrl: './promises.component.html',
  styles: []
})
export class PromisesComponent implements OnInit {

  constructor() {

    this.clock().then((data) => {
      console.log(data);
    }).catch((e) => {
      console.log('Error', e);
    });
  }

  ngOnInit(): void {
  }

  clock(): Promise<boolean>{
    return  new Promise((resolve, reject) => {
      let c = 0;
      const clock = setInterval(() => {
        console.log(c);
        if (c === 3) {
          resolve(true);
          clearInterval(clock);
        }
        c++;
      }, 1000);
    });
  }
}
