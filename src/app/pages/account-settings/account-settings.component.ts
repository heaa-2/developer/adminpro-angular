import {Component, ElementRef, Inject, OnInit} from '@angular/core';
import {SettingsService} from '../../services/index';
import {DOCUMENT} from '@angular/common';
@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html',
  styles: []
})
export class AccountSettingsComponent implements OnInit {

  constructor(
    @Inject(DOCUMENT) private html,
    public setting: SettingsService
  ) { }

  ngOnInit(): void {
    this.selectedCheck();
  }
  changeColor(theme: string, link: any ) {
    this.selectdCheck(link);
    this.setting.setSettings(theme);
  }
  selectdCheck(link: any) {
    const selectors: any =  this.html.getElementsByClassName('selector');

    for (const ref of selectors) {
      ref.classList.remove('working');
    }
    link.classList.add( 'working');
  }
  selectedCheck() {
    const selectors: any =  this.html.getElementsByClassName('selector');
    const theme = this.setting.settings.theme;
    for (const ref of selectors) {
        if (ref.getAttribute('data-theme') === theme) {
          ref.classList.add( 'working');
          return;
        }
    }
  }
}
