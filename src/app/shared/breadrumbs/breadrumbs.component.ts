import { Component, OnInit } from '@angular/core';
import {Router, ActivationEnd} from '@angular/router';
import {filter, map} from 'rxjs/operators';
import {Meta, MetaDefinition, Title} from '@angular/platform-browser';
@Component({
  selector: 'app-breadrumbs',
  templateUrl: './breadrumbs.component.html',
  styles: []
})
export class BreadrumbsComponent implements OnInit {

  breadrumbs: any;
  constructor(private  router: Router, private title: Title, private meta: Meta) {
    this.getDataRouter().
    subscribe( event => {
      this.title = event.title;
      title.setTitle(event.title);
      const tags: MetaDefinition = {
        name: 'description',
        content: event.title
      };
      meta.updateTag(tags);
      this.breadrumbs = event.title;
    });
  }

ngOnInit(): void {
  }

getDataRouter() {
    return this.router.events.
        pipe(
          filter( event => event instanceof ActivationEnd),
          filter( (event: ActivationEnd) => event.snapshot.firstChild === null),
          map((value: ActivationEnd) => value.snapshot.data)
        );
  }
}
