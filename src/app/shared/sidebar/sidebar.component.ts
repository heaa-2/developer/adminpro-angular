import {Component, OnInit} from '@angular/core';
import {AuthService, SidebarService} from '../../services';
import {User} from '../../models/user.model';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styles: []
})
export class SidebarComponent implements OnInit {

  user: User;
  constructor(
    public sidebar: SidebarService,
    public authService: AuthService,
  ) {
  }

  ngOnInit(): void {
    this.user = this.authService.user;
  }

  logOut() {
    this.authService.logOut();

  }
}
