import {NgModule} from '@angular/core';

import {BreadrumbsComponent} from './breadrumbs/breadrumbs.component';
import {HeaderComponent} from './header/header.component';
import {SidebarComponent} from './sidebar/sidebar.component';
import {NopagefoundComponent} from './nopagefound/nopagefound.component';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {PipesModule} from '../pipes/pipes.module';


@NgModule({
  declarations: [
    NopagefoundComponent,
    BreadrumbsComponent,
    HeaderComponent,
    SidebarComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    PipesModule
  ],
  exports: [
    NopagefoundComponent,
    BreadrumbsComponent,
    HeaderComponent,
    SidebarComponent
  ]
})
export class  SharedModule { }
