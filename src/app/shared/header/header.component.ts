import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services';
import {User} from '../../models/user.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: []
})
export class HeaderComponent implements OnInit {

  user: User;
  constructor(
    public authService: AuthService,
  ) {
  }

  ngOnInit(): void {
  this.user = this.authService.user;
  }

  logOut() {
    this.authService.logOut();
  }
}
