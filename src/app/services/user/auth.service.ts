import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {User} from '../../models/user.model';
import {map} from 'rxjs/operators';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: User;
  token: string;

  constructor(
    private http: HttpClient,
    public router: Router
  ) {
    this.loadStorge();
  }

  isAuth(): boolean {
    return (this.token.length > 5);
  }

  saveStorage(id: string, token: string, user: User) {
    localStorage.setItem('id', id);
    localStorage.setItem('token', token);
    localStorage.setItem('user', JSON.stringify(user));
    this.user = user;
    this.token = token;
  }

  loadStorge() {
    if (localStorage.getItem('token')) {
      this.token = localStorage.getItem('token');
      this.user = JSON.parse(localStorage.getItem('user'));
    } else {
      this.token = '';
      this.user = null;
    }
  }

  auth(user: User, remember: boolean = false) {
    if (remember) {
      localStorage.setItem('email', user.email);
    } else {
      localStorage.removeItem('email');
    }
    return this.http.post(environment.url_server + 'auth', user).pipe(
      map((res: any) => {
        this.saveStorage(res._id, res.token, res.user);
        return true;
      })
    );
  }


  authGoogle(token: string) {
    const url = environment.url_server + 'auth/google';
    return this.http.post(url, {token}).pipe(
      map((res: any) => {

        this.saveStorage(res._id, res.token, res.user);
        return true;
      })
    );
  }

  logOut() {
    localStorage.removeItem('token');
    localStorage.removeItem('id');
    localStorage.removeItem('user');
    this.token = '';
    this.user = null;
    this.router.navigate(['/login']);
  }
}
