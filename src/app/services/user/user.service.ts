import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';

import {User} from '../../models/user.model';
import {HttpClient} from '@angular/common/http';

import {map} from 'rxjs/operators';
import { UploadFileService } from '../upload/upload-file.service';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  user: User;
  token: string;

  constructor(
    public http: HttpClient,
    public authService: AuthService,
    public uploadFileService: UploadFileService
  ) {
    this.user = this.authService.user;
    this.token = this.authService.token;
  }

  create(user: User) {
    return this.http.post(environment.url_server + 'users', user);
  }

  update(user: User) {
    return this.http.put(environment.url_server + 'users/' + user._id + '?token=' + this.token, user)
      .pipe(
        map((res: any) => {
          this.authService.user = res.userSave;
          this.authService.saveStorage(this.user._id, this.token, res.userSave);
          return res.userSave;
        })
      );
  }

  updateAvatar(file: File, id: string) {
    this.uploadFileService.upload(file, 'user', id).then( (res: any) => {
     this.user.img = res.user.img;
     this.authService.user = res.user;
     this.authService.saveStorage(this.user._id, this.token, res.user);
     return res;
    }).catch(err => err);
  }
}
