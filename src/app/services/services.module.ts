import {NgModule} from '@angular/core';
import {
  AuthService,
  SettingsService,
  SharedService,
  SidebarService,
  UserService,
  UploadFileService
} from './index';


@NgModule({
  declarations: [],
  providers: [
    SettingsService,
    SharedService,
    SidebarService,
    UserService,
    AuthService,
    UploadFileService
  ],
  imports: []
})
export class ServicesModule {}
