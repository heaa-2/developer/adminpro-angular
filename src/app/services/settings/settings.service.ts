import {Inject, Injectable} from '@angular/core';
import {DOCUMENT} from '@angular/common';
@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  settings: Settings = {
    urlTheme:  'assets/css/colors/default-dark.css',
    theme:  'default'
  };
  constructor(
    @Inject(DOCUMENT) private html
  ) {
    this.getSettings();
  }
  setSettings(theme: string) {
    this.settings.theme = theme;
    this.settings.urlTheme = `assets/css/colors/${ theme }.css`;
    this.html.getElementById('theme').setAttribute('href', this.settings.urlTheme);
    localStorage.setItem('settings', JSON.stringify(this.settings));
  }
  getSettings() {

    if (localStorage.getItem('settings')) {
      this.settings =  JSON.parse(localStorage.getItem('settings'));

      this.html.getElementById('theme').setAttribute('href', this.settings.urlTheme);
    } else {

      this.html.getElementById('theme').setAttribute('href', this.settings.urlTheme);
    }
    console.log(this.settings);
  }
}
interface Settings {
  urlTheme: string;
  theme: string;
}
