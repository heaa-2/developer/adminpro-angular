import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'app-increase',
  templateUrl: './increase.component.html',
  styles: []
})
export class IncreaseComponent implements OnInit {
  @ViewChild('txtPercentage') txtPercentage: ElementRef;
  @Input() percentage: number;
  @Input() title: string;
  @Output() changePercentage: EventEmitter<number> = new EventEmitter<number>();
  min: number;
  max: number;
  constructor() {
    this.max = 100;
    this.min = 0;
  }

  ngOnInit(): void {
  }
  changeValue( v ) {
    this.percentage += v;
    if (this.percentage >= this.max) {
      this.percentage = 100;
    } else if (this.percentage <= this.min) {
      this.percentage = 0;
    }
    this.changePercentage.emit( this.percentage );
    this.txtPercentage.nativeElement.focus();
  }
  onChanges(newValue: number) {

    if (newValue >= this.max) {
      this.percentage = 100;
    } else if (newValue <= this.min) {
      this.percentage = 0;
    } else {
      this.percentage = newValue;
    }
    this.txtPercentage.nativeElement.value = this.percentage;
    this.changePercentage.emit(this.percentage);
    this.txtPercentage.nativeElement.focus();
  }
}
