import {NgModule} from '@angular/core';
import {IncreaseComponent} from './increase/increase.component';
import {FormsModule} from '@angular/forms';
import { ChartDoughnutComponent } from './chart-doughnut/chart-doughnut.component';
import {ChartsModule} from 'ng2-charts';


@NgModule({
  declarations: [
    IncreaseComponent,
    ChartDoughnutComponent
  ],
  imports: [
    FormsModule,
    ChartsModule
  ],
  exports: [
    IncreaseComponent,
    ChartDoughnutComponent
  ]
})
export class ComponentsModule {}
