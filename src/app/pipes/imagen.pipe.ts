import {Pipe, PipeTransform} from '@angular/core';
import {environment} from '../../environments/environment';

@Pipe({
  name: 'imagen'
})
export class ImagenPipe implements PipeTransform {

  transform(img: string, collection: any): any {
    const urlBase = environment.url_server + 'img/';
    if (!img) {
      return urlBase + 'user/xxx';
    }
    if ( !(img.indexOf('https://') === -1) ) {
      return img;
    }
    return `${urlBase}${collection}/${img}`;
  }
}
